## Author: Dmitry Kurdenok
## E-mail: kurdenok@ngs.ru
## Novosibirsk. July, 2018

import sys
import argparse
import graphviz

print("Author: Dmitry Kurdenok")
print("E-mail: kurdenok@ngs.ru")
print("Novosibirsk. July, 2018")

def opponent(player):
    return 'I' if player == 'II' else 'II'

class GameRules:
    def __init__(self, available_moves, end_condition, win_condition):
        self.available_moves = available_moves
        self.end_condition = end_condition
        self.win_condition = win_condition

class GameTree:
    next_id = 0

    def default_get_single(children):
        return children[-1:]

    def __init__(self, game, player, position):
        self.game = game
        self.player = player
        self.position = position
        self.children = []
        self.winner = None
        self.id = GameTree.next_id
        GameTree.next_id += 1

    def get_full_tree(game, player, position):
        return GameTree.construct_full_tree(game, player, position)
    
    def get_big_win_tree(game, player, position):
        tree = GameTree.construct_full_tree(game, player, position)
        tree.shrink_to_big_win_tree()
        return tree
    
    def get_small_win_tree(game, player, position, get_single = default_get_single):
        tree = GameTree.construct_full_tree(game, player, position)
        tree.shrink_to_small_win_tree(get_single)
        return tree

    def get_digraph(self, name = "game", frmt = "png"):
        digraph = graphviz.Digraph(name, format = frmt, graph_attr = {"rankdir": "LR"})
        self.construct_digraph(digraph)
        return digraph

    def get_winner(self):
        if not self.winner:
            self.winner = opponent(self.player)
            for child in self.children:
                if child.get_winner() == self.player:
                    self.winner = self.player
                    break
        return self.winner

    def get_copy(self):
        copy = GameTree(self.game, self.player, self.position, winner = self.winner)
        for child in self.children:
            copy.children.append(child.get_copy())
        return copy
    
    def shrink_to_big_win_tree(self):
        winner = self.get_winner()
        new_children = []
        for child in self.children:
            if child.get_winner() == winner:
                child.shrink_to_big_win_tree()
                new_children.append(child)
        self.children = new_children

    def shrink_to_small_win_tree(self, get_single = default_get_single):
        winner = self.get_winner()
        if self.player == winner:
            new_children = []
            for child in self.children:
                if child.get_winner() == winner:
                    child.shrink_to_small_win_tree(get_single)
                    new_children.append(child)
            if len(new_children) != 0:
                self.children = get_single(new_children)
            else:
                self.children = new_children
        else:
            for child in self.children:
                child.shrink_to_small_win_tree(get_single)

    def merge_equal_nodes(self, table = None):
        if not table:
            table = dict()
        new_children = []
        for child in self.children:
            child_key = (child.player, child.position)
            if not list(table.keys()).count(child_key) != 0:
                table[child_key] = child
            new_children.append(table[child_key])
        self.children = new_children
        for child in self.children:
            child.merge_equal_nodes(table)

    def construct_full_tree(game, player, position):
        node = GameTree(game, player, position)
        if game.end_condition(position):
            node.winner = game.win_condition(player, position)
        else:
            for move in game.available_moves:
                node.children.append(GameTree.construct_full_tree(game, opponent(player), move(position)))
        return node

    def construct_digraph(self, digraph, edges = set()):
        parent_mark = str(self.id)
        if len(self.children) == 0:
            digraph.node(parent_mark, str(self.position) + "\n" + self.get_winner() + "win")
        else:
            digraph.node(parent_mark, str(self.position))
            for child in self.children:
                child_mark = str(child.id)
                child.construct_digraph(digraph, edges)
                edge_mark = (parent_mark, child_mark)
                if not edge_mark in edges:
                    digraph.edge(parent_mark, child_mark, style = "solid" if self.player == "I" else "dotted")
                    edges.add(edge_mark)

def move_plus_1(pos):
    return pos + 1

def move_plus_3(pos):
    return pos + 3

def move_mult_2(pos):
    return pos * 2

def at_least_36(pos):
    return pos >= 36

def last_move(player, position):
    return opponent(player)

moves_list = [move_plus_1, move_plus_3, move_mult_2]
game = GameRules(moves_list, at_least_36, last_move)
tree = GameTree.get_full_tree(game, "I", 4)
tree.shrink_to_small_win_tree()
tree.merge_equal_nodes()

digraph = tree.get_digraph()

digraph.render("output/game.gv")
